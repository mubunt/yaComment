# *yaComment*, *.comment* file generation and update.

## FIRST OF ALL...
Refer to  *yaTree* (https://gitlab.com/mubunt/yaTree) utility for *.comment* file description.
## LICENSE
**yaComment** is covered by the GNU General Public License (GPL) version 3 and above.
## USAGE
``` bash
$ yaComment --help
yaComment - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
yaComment - Version 1.2.0

.comment file generation and update (associated to 'yaTree' utility)

Usage: yaComment [OPTIONS]... [DIR]...

  -h, --help     Print help and exit
  -V, --version  Print version and exit
  -a, --all      Do not ignore entries starting with '.'.  (default=off)

Exit: returns a non-zero status if an error is detected.

$ yaComment
...
$ yaComment ~/Sandbox/src/app1 ~/Sandbox/src/app2
...
```
## STRUCTURE OF THE APPLICATION
This section walks you through **yaComment**'s structure. Once you understand this structure, you will easily find your way around in **yaComment**'s code base.
``` bash
$ yaTree
./                    # Application level
├── src/              # Source directory
│   ├── Makefile      # Makefile
│   ├── comment.c     # 'comment' package
│   ├── comment.h     # 'comment' package header file
│   ├── common.h      # Common definitions
│   ├── yaComment.c   # Main program
│   └── yaComment.ggo # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md        # GNU General Public License markdown file
├── LICENSE.md        # License markdown file
├── Makefile          # Makefile
├── README.md         # ReadMe markdown file
├── RELEASENOTES.md   # Release Notes markdown file
└── VERSION           # Version identification text file

1 directories, 12 files
$ 
```
## HOW TO BUILD THIS APPLICATION
### Linux Platform
``` bash
$ cd yaComment
$ make clean all
```
### Windows Platform
For the Windows platform, the application is built on Linux (cross-platform generation).
``` bash
$ cd yaComment
$ make wclean wall
``` 
## HOW TO RELEASE AND INSTALL AND USE THIS APPLICATION
### Linux Platform
``` bash
$ cd yaComment
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

### Windows Platform
Install *yaComment.exe* a mano.

## SOFTWARE REQUIREMENTS
- For usage:
   - Nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - MinGW-64 packages for Windows cross-generation
- Application developed and tested with XUBUNTU 18.04 and GCC v7.3.0
- Up to version 1.0.4, application tested with WINDOWS 10.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .
***
