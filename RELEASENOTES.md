# RELEASE NOTES: *yaComment*, *.comment* file generation and update (associated to 'yaTree' utility).

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.7**:
  - Updated build system components.

- **Version 1.2.6**:
  - Updated build system.

- **Version 1.2.5**:
  - Removed unused files.

- **Version 1.2.4**:
  - Updated build system component(s)

- **Version 1.2.3**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it

- **Version 1.2.2**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.2.1**:
  - Homogenization with *yaTree* on the management of unnamed options with *gengetopt*.

- **Version 1.2.0**:
  - Allowed target directory paths as launching parameter.

- **Version 1.1.4**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.1.3**:
  - Added tagging of new release.

- **Version 1.1.2**:
  - Improved (again) top level makefile.

- **Version 1.1.1**:
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.

- **Version 1.1.0**:
  - Moved from GPL v2 to GPL v3.
  - Replaced text files by markdown version: RELASENOTES, COPYING, LICENSE.
  - Added default description for these markdown files.
  - Some other improvements...

- **Version 1.0.4**:
  - Fixed typos in README.md file.

- **Version 1.0.3**:
  - Fixed typo in RELEASENOTES

- **Version 1.0.2**:
  - Removed HTML short description for GitHub ('index.html' file). Was a mistake; the README file do very well the job.
  - Improved "clean" target in str/Makefile.
  - Formatting and improving the README file.
  - Fixed compilation errors of Windows cross-compilation (!).

- **Version 1.0.1**:
  - Added HTML short description for GitHub ('index.html' file).

- **Version 1.0.0**:
  - First release.
