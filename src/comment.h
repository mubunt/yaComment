//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaComment
// .comment file generation and update (associated to 'yaTree' utility)
//------------------------------------------------------------------------------
#ifndef COMMENT_H
#define COMMENT_H
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern int _getExistingCommentFromFile(char *, struct s_item *p, size_t, struct s_item *, size_t);
extern bool _generateCommentFile(char *, struct s_item *, size_t, struct s_item *, size_t, size_t);
extern const char *_getFileDefaultDescription(char *);
extern const char *_getDirDefaultDescription(char *);
//------------------------------------------------------------------------------
#endif	// COMMENT_H
