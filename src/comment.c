//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaComment
// .comment file generation and update (associated to 'yaTree' utility)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "common.h"
#include "comment.h"
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
bool _generateCommentFile(char *currentdir,
                          struct s_item *ptFiles, size_t nbFiles,
                          struct s_item *ptDirs, size_t nbDirs,
                          size_t len) {
	char comment[2 * PATH_MAX];
	snprintf(comment, sizeof(comment), "%s/%s", currentdir, COMMENTFILE);
	unlink(comment);
	FILE *fdcomment;
	if (NULL == (fdcomment = fopen(comment, "w")))
		return FALSE;
	unsigned int idx;
	size_t n = ((len + TAB_SIZE) / TAB_SIZE) * TAB_SIZE;
	for (idx = 0; idx < nbFiles; idx++) {
		if (ptFiles[idx].takeit == TRUE) {
			fprintf(fdcomment, "%s", ptFiles[idx].name);
			size_t k = (n - strlen(ptFiles[idx].name)) / TAB_SIZE;
			for (size_t i = 0; i < (k + (((n - strlen(ptFiles[idx].name)) % TAB_SIZE) == 0 ? 0 : 1)) ; i++)
				fprintf(fdcomment, "\t");
			fprintf(fdcomment, "%s\n", ptFiles[idx].description);
		}
	}
	for (idx = 0; idx < nbDirs; idx++) {
		if (ptDirs[idx].takeit == TRUE) {
			fprintf(fdcomment, "%s", ptDirs[idx].name);
			size_t k = (n - strlen(ptDirs[idx].name)) / TAB_SIZE;
			for (size_t i = 0; i < (k + (((n - strlen(ptDirs[idx].name)) % TAB_SIZE) == 0 ? 0 : 1)) ; i++)
				fprintf(fdcomment, "\t");
			fprintf(fdcomment, "%s\n", ptDirs[idx].description);
		}
	}
	fclose(fdcomment);
	return TRUE;
}
//-----------------------------------------------------------------------------
static bool existFile(char *file) {
	struct stat locstat;
	if (file == NULL) return FALSE;
	if (stat(file, &locstat) < 0) return FALSE;
	return TRUE;
}
static char *lookForObject(struct s_item *ptObj, size_t nbObj, char *name) {
	for (int idx = 0; idx < (int)nbObj; idx++) {
		if (strcmp(ptObj[idx].name, name) == 0) return ptObj[idx].description;
	}
	return (char *)NULL;
}

int _getExistingCommentFromFile(char *currentdir,
                                struct s_item *ptFiles, size_t nbFiles,
                                struct s_item *ptDirs, size_t nbDirs) {
	char comment[2 * PATH_MAX];
	snprintf(comment, sizeof(comment), "%s/%s", currentdir, COMMENTFILE);
	if (! existFile(comment)) return 0;
	FILE *file;
	if (NULL == (file = fopen(comment, "r"))) return -1;

	char line[DESCR_MAX + 1];
	char *ptfound = NULL;
	int l = 0;
	while (fgets(line, sizeof(line), file)) {
		++l;
		if (line[strlen(line) - 1] == '\n') line[strlen(line) - 1] = '\0';
		if (line[0] == '\0') continue;
		if (line[0] == COMMENT) continue;
		int i = 0;
		if (line[0] == ' ' || line[0] == '\t') { // Continuation of the previous comment
			if (ptfound == NULL) {
				fclose(file);
				return l;
			}
			while (line[i] == ' ' || line[i] == '\t') ++i;
			strncat(ptfound, CONTINUATION_LINE, DESCR_MAX);
			strncat(ptfound, line + i, DESCR_MAX);
			continue;
		}
		while (line[i] != ' ' && line[i] != '\t' && line[i] != '\0') ++i;
		if (line[i] != '\0') {
			line[i++] = '\0';
			while ((line[i] == ' ' || line[i] == '\t') && line[i] != '\0') ++i;
		}
		if (NULL != (ptfound = lookForObject(ptFiles, nbFiles, line)))
			strncpy(ptfound, line + i, DESCR_MAX);
		else if (NULL != (ptfound = lookForObject(ptDirs, nbDirs, line)))
			strncpy(ptfound, line + i, DESCR_MAX);
	}
	fclose(file);
	return 0;
}
//-----------------------------------------------------------------------------
static char *_getFilenameExtension(char *filename) {
	char *dot = strrchr(filename, '.');
	if (!dot || dot == filename) return (char *)"";
	return dot + 1;
}

const char *_getFileDefaultDescription(char *name) {
	if (strcmp(name, ".gitignore") == 0) return "GIT file specifying intentionally untracked files to ignore";
	if (strcmp(name, "COPYING") == 0) return "GNU General Public License text file";
	if (strcmp(name, "COPYING.md") == 0) return "GNU General Public License markdown file";
	if (strcmp(name, "LICENSE") == 0) return "License text file";
	if (strcmp(name, "LICENSE.md") == 0) return "License markdown file";
	if (strcmp(name, "makefile") == 0) return "Makefile";
	if (strcmp(name, "Makefile") == 0) return "Makefile";
	if (strcmp(name, "README") == 0) return "ReadMe Text file";
	if (strcmp(name, "README.md") == 0) return "ReadMe markdown file";
	if (strcmp(name, "RELEASENOTES") == 0) return "Release Notes text file";
	if (strcmp(name, "RELEASENOTES.md") == 0) return "Release Notes markdown file";
	if (strcmp(name, "VERSION") == 0) return "Version identification text file";
	if (strcmp(_getFilenameExtension(name), "ggo") == 0) return "'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html";
	return "";
}
const char *_getDirDefaultDescription(char *name) {
	if (strcmp(name, "src") == 0) return "Source directory";
	if (strcmp(name, "bin") == 0) return "Binary directory";
	if (strcmp(name, "linux") == 0) return "Linux Binary directory";
	if (strcmp(name, "LINUX") == 0) return "Linux Binary directory";
	if (strcmp(name, "windows") == 0) return "Windows Binary directory";
	if (strcmp(name, "WINDOWS") == 0) return "Windows Binary directory";
	if (strcmp(name, "README_images") == 0) return "Images for documentation";
	return "";
}
//-----------------------------------------------------------------------------
