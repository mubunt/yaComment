//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaComment
// .comment file generation and update (associated to 'yaTree' utility)
//------------------------------------------------------------------------------
#ifndef COMMON_H
#define COMMON_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdarg.h>
#ifdef LINUX
#include <linux/limits.h>
#include <unistd.h>
#else
#include <limits.h>
#endif
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#ifndef max
#define max(a,b)				((a) > (b) ? (a) : (b))
#endif

#define GITDIR					".git"
#define COMMENTFILE				".comment"
#define COMMENT 				'#'
#define CONTINUATION_LINE		"\n\t"
#define TAB_SIZE				4
#define DESCR_MAX				160
//------------------------------------------------------------------------------
// TYPEDEF
//------------------------------------------------------------------------------
typedef enum { FALSE, TRUE } boolean;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_item {
	char name[PATH_MAX];
	bool takeit;
	bool toscan;
	char description[DESCR_MAX];
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern unsigned int debug_mode;
//------------------------------------------------------------------------------
#endif	// COMMON_H
