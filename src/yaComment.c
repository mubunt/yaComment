//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaComment
// .comment file generation and update (associated to 'yaTree' utility)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "common.h"
#include "comment.h"
#include "yaComment_cmdline.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define ESC 						0x1b
#define printbold(color, fmt, ...)	fprintf(stdout, "%c[1;%dm" fmt "%c[0m", ESC, color, __VA_ARGS__, ESC)
#define print(color, fmt, ...)		fprintf(stdout, "%c[0;%dm" fmt "%c[0m", ESC, color, __VA_ARGS__, ESC)
#define COLOR_TRAP					31	// RED
#define COLOR_INFORMATION			37	// WHITE

#define ENTERDIR					0
#define LEAVEDIR					1
#ifndef LINUX
#define	lstat 						stat
#endif
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
unsigned int debug_mode = 0;
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void printfatal(const char *format, ...) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	printbold(COLOR_TRAP, "%s", buff);
	va_end(argp);
	exit(EXIT_FAILURE);
}
//-----------------------------------------------------------------------------
static void _sighandler(int sig) {
	signal(SIGINT, SIG_DFL);
#ifdef LINUX
	char *str = strdup(strsignal(sig));
	char *ptstr = str;
	while (*ptstr != '\0') {
		*ptstr = (char) toupper(*ptstr);
		++ptstr;
	}
	printfatal("\n!!! Signal %s catched !!! Exit.\n\n", str);
#else
	printfatal("\n!!! Signal %d catched !!! Exit.\n\n", sig);
#endif
}
//-----------------------------------------------------------------------------
static void printerror(const char *format, ...) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	print(COLOR_INFORMATION, "%s\n", buff);
}
//-----------------------------------------------------------------------------
static void printlocation(char *dir) {
	char buff[PATH_MAX];
	snprintf(buff, PATH_MAX, "Directory %s", dir);
	print(COLOR_INFORMATION, "%s\n", buff);
}
//-----------------------------------------------------------------------------
static void _scanning(char *targetdir) {
	printlocation(targetdir);

	DIR *pDir;
	if ((pDir = opendir(targetdir))  == NULL) {
		printerror("Cannot open directory '%s'", targetdir);
		return;
	}
	// Step 1: Count the number of directories and regular files.
	struct dirent *pDirent;
	char entry[2 * PATH_MAX];
	struct stat sb;
	size_t nbDirs = 0;
	size_t nbFiles = 0;
	size_t maxEntryLen = 0;

	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (strcmp(pDirent->d_name, ".") == 0) continue;
		if (strcmp(pDirent->d_name, "..") == 0) continue;
		if (strcmp(pDirent->d_name, COMMENTFILE) == 0) continue;
		if (strcmp(pDirent->d_name, GITDIR) == 0) continue;
		if (pDirent->d_name[0] == '.' && ! args_info.all_given) continue;
		sprintf(entry, "%s/%s", targetdir, pDirent->d_name);
		if (lstat(entry, &sb) < 0)  {
			printerror("Cannot stat %s\n", entry);
			continue;
		}
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFDIR)
			++nbDirs;
		else
			++nbFiles;
		maxEntryLen = max(maxEntryLen, strlen(pDirent->d_name));
	}

	if (nbDirs != 0 || nbFiles != 0)  {
		// Step 2: Allocate tables for directories and regular files
		struct s_item *ptDirs = NULL;
		if (nbDirs != 0) {
			if (NULL == (ptDirs = (struct s_item *)calloc(nbDirs, sizeof(struct s_item)))) {
				printerror("%s", "Cannot allocate memory for directories");
				closedir(pDir);
				return;
			}
		}
		struct s_item *ptFiles = NULL;
		if (nbFiles != 0) {
			if (NULL == (ptFiles = (struct s_item *)calloc(nbFiles, sizeof(struct s_item)))) {
				printerror("%s", "Cannot allocate memory for regular files");
				free(ptDirs);
				closedir(pDir);
				return;
			}
		}

		// Step 3: Fill allocated tables from current directory
		unsigned int iDirs = 0, iFiles = 0;
		rewinddir(pDir);
		while ((pDirent = readdir(pDir)) != NULL) {
			if (strlen(pDirent->d_name) == 0) continue;
			if (strcmp(pDirent->d_name, ".") == 0) continue;
			if (strcmp(pDirent->d_name, "..") == 0) continue;
			if (strcmp(pDirent->d_name, COMMENTFILE) == 0) continue;
			if (strcmp(pDirent->d_name, GITDIR) == 0) continue;
			if (pDirent->d_name[0] == '.' && ! args_info.all_given) continue;
			sprintf(entry, "%s/%s", targetdir, pDirent->d_name);
			if (lstat(entry, &sb) < 0)  {
				printerror("Cannot stat %s\n", entry);
				continue;
			}
			mode_t mode = sb.st_mode & S_IFMT;
			if (mode == S_IFDIR) {
				if (nbDirs != 0) {
					strncpy(ptDirs[iDirs].name, pDirent->d_name, PATH_MAX);
					ptDirs[iDirs].takeit = TRUE;
					ptDirs[iDirs].toscan = TRUE;
					strncpy(ptDirs[iDirs].description, _getDirDefaultDescription(pDirent->d_name), DESCR_MAX);
					iDirs++;
				}
			} else {
				if (nbFiles != 0) {
					strncpy(ptFiles[iFiles].name, pDirent->d_name, PATH_MAX);
					ptFiles[iFiles].takeit = TRUE;
					ptFiles[iFiles].toscan = FALSE;
					strncpy(ptFiles[iFiles].description, _getFileDefaultDescription(pDirent->d_name), DESCR_MAX);
					iFiles++;
				}
			}
		}
		closedir(pDir);

		// Step 4: Complete allocated  tables with information from .comment file
		int n;
		if (0 != (n = _getExistingCommentFromFile(targetdir, ptFiles, nbFiles, ptDirs, nbDirs))) {
			if (n == -1)
				printerror("Cannot open file %s/%s", targetdir, COMMENTFILE);
			else
				printerror("Syntax error in file '%s/%s', line %d: No file name", targetdir, COMMENTFILE, n);
			free(ptFiles);
			free(ptDirs);
			return;
		}

		// Step 5: Generate ".comment" file
		if (FALSE == _generateCommentFile(targetdir,
		                                  ptFiles, nbFiles, ptDirs, nbDirs, maxEntryLen)) {
			printerror("Cannot create file %s/%s", targetdir, COMMENTFILE);
			free(ptFiles);
			free(ptDirs);
			return;
		}
		free(ptFiles);

		// Step 6: Go to next levels
		unsigned int idx;
		for (idx = 0; idx < nbDirs; idx++) {
			if (ptDirs[idx].takeit == FALSE || ptDirs[idx].toscan == FALSE) continue;
			snprintf(entry, sizeof(entry), "%s/%s", targetdir, ptDirs[idx].name);
			_scanning(entry);
		}
		free(ptDirs);
	}
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat locstat;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_yaComment(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//--- Trap -----------------------------------------------------------------
	signal(SIGABRT, _sighandler);
	signal(SIGTERM, _sighandler);
	signal(SIGINT, _sighandler);
	//--- Go on ----------------------------------------------------------------
	if (args_info.inputs_num == 0) {
		_scanning((char *)".");
	} else {
		for (unsigned int i = 0 ; i < args_info.inputs_num ; i++) {
			_scanning(args_info.inputs[i]);
		}
	}

	//--- Exit -----------------------------------------------------------------
	cmdline_parser_yaComment_free(&args_info);
	return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------------
